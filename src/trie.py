#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from itertools import chain
from operator import itemgetter


class _nd(dict):
    "Internal classe, implements trie nodes"
    def __init__(self):
        self.n = 0

    def suffixes(self, acc):
        if None in self and acc:
            yield acc, self[None].n
        for c in self.keys():
            if c is None:
                continue
            yield from self[c].suffixes(acc + c)

            

class Trie:
    """
    Implements trie dictionnary.
    (particularity of this trie is it memorize 
    how often an item has stored in it)

    Exemples:
    >>> wlst = ['test', 'tesson', 'talon', 'tennis', 'torche', 'testa']
    >>> trie = Trie(wlst)
    >>> sorted(trie.suffixes('te'))
    ['nnis', 'sson', 'st', 'sta']
    >>> all([w in trie for w in wlst])
    True
    >>> [w in trie for w in ['test*', 'autres', 'mots', 'tes']]
    [False, False, False, False]
    >>> trie.add('test')
    >>> trie.get_hfreqs('test')
    [1.0, 0.7142857142857143, 0.8, 0.75, 0.6666666666666666]
    >>> trie.get_hfreqs('mots')
    """

    def __init__(self, wrdlst=None):
        "constructor of Trie object"
        self._root = _nd()
        if wrdlst != None:
            for w in wrdlst:
                self.add(w)
        
    def add(self, word):
        "add word to trie"
        nd = self._root
        nd.n += 1
        for c in chain(word, [None]):
            if c not in nd:
                nd[c] = _nd()
            nd = nd[c]
            nd.n += 1

    def suffixes(self, word):
        "return list of word suffixes, ordered by frequency"
        nd = self._root
        for c in word:
            if c not in nd:
                return
            nd = nd[c]
        suf, _ = zip(*sorted(nd.suffixes(''), reverse=True, key=itemgetter(1)))
        return list(suf)

    def __contains__(self, word):
        "test if word is in trie"
        nd = self._root
        for c in word:
            if c not in nd:
                return False
            nd = nd[c]
        return None in nd

    def get_hfreqs(self, word):
        "get elements relative frequencies"
        if word not in self:
            return
        ret = []
        nd = self._root
        for c in word:
            ret.append(nd[c].n / nd.n)
            nd = nd[c]
        ret.append(nd[None].n / nd.n)
        return ret


if __name__ == '__main__':
    import doctest
    doctest.testmod()
