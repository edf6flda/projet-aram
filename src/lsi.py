#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from time import time
from collections import OrderedDict
from heapq import nlargest
from numpy import array
from pprint import pprint

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.metrics.pairwise import cosine_similarity

from corpus import Corpus
from interface_settings import OptionsDialog, Options
from tokenizer import AramTokenizer

LOG_ALL = False


def print_numpy_info(raw_similitudes, prefix):
    if LOG_ALL:
        print("")
        print("%s size : " % prefix, raw_similitudes.size)
        print("")
        print("%s : " % prefix)
        pprint(raw_similitudes)
        print("")


class Index:

    """Vectorise les documents par TF-IDF, puis réduit l'espace vectoriel par LSI."""

    def __init__(self, nb_lsi_features=250, verbose=True):
        self.documents = OrderedDict()
        self.vectorizer = TfidfVectorizer(sublinear_tf=True,
                                 use_idf=True,
                                 tokenizer=AramTokenizer,
                                 ngram_range=(1,1))
        self.lsa = TruncatedSVD(n_components=nb_lsi_features)
        self.verbose = verbose
        
    def add_document(self, doc, content):
        self.documents[doc] = content
        
    def indexing(self):
        t0 = time()
        self.tfidf_space = self.vectorizer.fit_transform(self.documents.values())
        if self.verbose:
            print("TF-IDF extraction of {}. elapsed {:.4}s".\
                    format(self.tfidf_space.shape, time()-t0))
        
        t0 = time()
        self.lsa_space = self.lsa.fit_transform(self.tfidf_space)
        if self.verbose:
            print("LSI reduction of {}. elapsed {:.4}s".\
                    format(self.lsa_space.shape, time()-t0))

    def search(self, query, using_lsa=Options.get_use_lsi_pref(), n=10):
        query_space = self.vectorizer.transform([query])
        if using_lsa:
            query_space = self.lsa.transform(query_space)
        index_space = self.lsa_space if using_lsa else self.tfidf_space
        raw_similitudes = cosine_similarity(query_space, index_space)
        """0.0 means usually not found"""
        min_cosinus = Options.get_min_cosinus_pref()
        constrained_similitudes = array([x for x in raw_similitudes[0] if min_cosinus < abs(x)])
        print_numpy_info(raw_similitudes, "raw_similitudes")
        print_numpy_info(constrained_similitudes, "constrained_similitudes")

        best_matches = nlargest(n, range(constrained_similitudes.size), key=lambda k: constrained_similitudes[k])
        keys = list(self.documents.keys())
        if LOG_ALL:
            print("len(best_matches)", len(best_matches))
            print("best_matches : ", best_matches)

        matches = [keys[i] for i in best_matches]
        if LOG_ALL:
            print("")
            print("matches : ")
            pprint(matches)

        return matches


if __name__ == '__main__':
    from pprint import pprint
    
    corpora = Corpus.get_all_corpus()[0]
    index = Index()

    for src in corpora.sources.values():
        for sec_title, sec_words in zip(src.section_titles, src.sections):
            index.add_document((src.title, sec_title), sec_words)
    
    index.indexing()

    q = "𐡀𐡁𐡉𐡉 𐡓𐡁𐡀 𐡉𐡀𐡅𐡔 𐡔𐡋𐡀 𐡌𐡃𐡏𐡕"

    res = index.search(q, False)
    if LOG_ALL:
        pprint(res)

    res = index.search(q, True)
    if LOG_ALL:
        pprint(res)
