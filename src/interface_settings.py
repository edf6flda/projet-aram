#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# __author__ = 'VR'
import PyQt4
from PyQt4.QtCore import QSettings
from PyQt4.QtGui import QVBoxLayout, QPushButton, QSizePolicy, QLineEdit, QLabel, QCheckBox


USER_PREF = 'user_pref'

WORDS_NUMBER_PREF = 'max_words_count'
PREF_DEFAULT_RESULTS_COUNT = 20
USE_LSI_PREF = 'use_lsi'
PREF_DEFAULT_USE_LSI = True
MIN_COSINUS_PREF = 'min_cosinus'


def get_default_min_cosinus():
    if PREF_DEFAULT_USE_LSI:
        return 0.3
    else:
        return 0.05


PREF_DEFAULT_MIN_COSINUS = get_default_min_cosinus()

NUMBER_TEST_WORDS = 'number_test_words'
PREF_DEFAULT_NUM_TEST_WORDS = 6


class OptionsDialog(PyQt4.QtGui.QDialog):

    def add_item(self, vbox, results_, edit, text):
        label = QLabel(results_, self)
        edit.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        edit.setText(text)
        label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        vbox.addWidget(label)
        vbox.addWidget(edit)

    def __init__(self, parent):
        super(OptionsDialog, self).__init__(parent)
        self.setWindowTitle('Settings')
        vbox = QVBoxLayout(self)
        btn = QPushButton('Confirm', self)
        btn.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        btn.clicked.connect(self.btn_clicked)

        self.results_nb_edit = QLineEdit(self)
        self.test_words_nb_edit = QLineEdit(self)
        self.min_cosinus_edit = QLineEdit(self)
        self.add_item(vbox, 'Number of results: ', self.results_nb_edit, str(Options.get_max_word_results_pref()))
        self.add_item(vbox, 'Number of test words: ', self.test_words_nb_edit, str(Options.get_test_words_number_pref()))
        self.add_item(vbox, 'Max cosinus: ', self.min_cosinus_edit, str(Options.get_min_cosinus_pref()))
        self.cb = QCheckBox('Use lsi', self)
        self.cb.move(20, 20)
        if Options.get_use_lsi_pref():
            self.cb.toggle()

        vbox.addWidget(self.cb)
        vbox.addWidget(btn)
        # Options.read_settings()
        # Options.save_settings()
        # Options.read_settings()

    def btn_clicked(self):
        number_of_results = int(self.results_nb_edit.text())
        number_of_test_words = int(self.test_words_nb_edit.text())
        min_cosinus = float(self.min_cosinus_edit.text())
        use_lsi = self.cb.isChecked()
        Options.save_settings(number_of_results, use_lsi, min_cosinus, number_of_test_words)
        Options.read_settings()
        self.hide()


class Options():

    @staticmethod
    def save_settings():
        settings = QSettings(USER_PREF, USER_PREF)
        settings.setValue(WORDS_NUMBER_PREF, PREF_DEFAULT_RESULTS_COUNT)
        settings.setValue(USE_LSI_PREF, PREF_DEFAULT_USE_LSI)
        settings.setValue(MIN_COSINUS_PREF, PREF_DEFAULT_MIN_COSINUS)
        settings.setValue(NUMBER_TEST_WORDS, PREF_DEFAULT_NUM_TEST_WORDS)
        # This will write the setting to the platform specific storage.
        del settings

    @staticmethod
    def save_settings(results_count, use_lsi, min_cosinus, num_test_words):
        settings = QSettings(USER_PREF, USER_PREF)
        settings.setValue(WORDS_NUMBER_PREF, results_count)
        settings.setValue(USE_LSI_PREF, use_lsi)
        settings.setValue(MIN_COSINUS_PREF, min_cosinus)
        settings.setValue(NUMBER_TEST_WORDS, num_test_words)
        # This will write the setting to the platform specific storage.
        del settings


    @staticmethod
    def get_use_lsi_pref():
        settings = QSettings(USER_PREF, USER_PREF)
        try:
            bool_value = settings.value(USE_LSI_PREF, PREF_DEFAULT_USE_LSI, type=bool)
            print(USE_LSI_PREF + " : %s" % repr(bool_value))
            return bool_value
        except TypeError as e:
            print(e)

    @staticmethod
    def get_max_word_results_pref():
        settings = QSettings(USER_PREF, USER_PREF)
        try:
            int_value = settings.value(WORDS_NUMBER_PREF, PREF_DEFAULT_RESULTS_COUNT, type=int)
            print(WORDS_NUMBER_PREF + " : %s" % repr(int_value))
            return int_value
        except TypeError as e:
            print(e)

    @staticmethod
    def get_min_cosinus_pref():
        settings = QSettings(USER_PREF, USER_PREF)
        try:
            float_value = settings.value(MIN_COSINUS_PREF, PREF_DEFAULT_MIN_COSINUS, type=float)
            print(MIN_COSINUS_PREF + " : %s" % repr(float_value))
            return float_value
        except TypeError as e:
            print(e)

    @staticmethod
    def get_test_words_number_pref():
        settings = QSettings(USER_PREF, USER_PREF)
        try:
            int_value = settings.value(NUMBER_TEST_WORDS, PREF_DEFAULT_NUM_TEST_WORDS, type=int)
            print(NUMBER_TEST_WORDS + " : %s" % repr(int_value))
            return int_value
        except TypeError as e:
            print(e)

    @staticmethod
    def read_settings():
        Options.get_max_word_results_pref()
        Options.get_min_cosinus_pref()
        Options.get_use_lsi_pref()
        Options.get_test_words_number_pref()