#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def get_ngrams(iterable, n):
    """
    Generator: yiels all n-grams from iterable

    Exemples:
    >>> lst = 'il etait une fois un marchand de foi'.split()
    >>> list(get_ngrams(lst, 2))
    [('il', 'etait'), ('etait', 'une'), ('une', 'fois'), ('fois', 'un'), ('un', 'marchand'), ('marchand', 'de'), ('de', 'foi')]
    >>> list(get_ngrams(lst, 3))
    [('il', 'etait', 'une'), ('etait', 'une', 'fois'), ('une', 'fois', 'un'), ('fois', 'un', 'marchand'), ('un', 'marchand', 'de'), ('marchand', 'de', 'foi')]
    >>> list(get_ngrams(lst, 4))
    [('il', 'etait', 'une', 'fois'), ('etait', 'une', 'fois', 'un'), ('une', 'fois', 'un', 'marchand'), ('fois', 'un', 'marchand', 'de'), ('un', 'marchand', 'de', 'foi')]
    >>> list(get_ngrams(lst, 10))
    []
    """
    lst = list(iterable)
    for i in range(len(lst) - n + 1):
        yield tuple(lst[i: i+n])


if __name__ == '__main__':
    import doctest
    doctest.testmod()

