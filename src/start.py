#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# __author__ = 'VR'


from PyQt4 import QtGui
import sys

from interface_qt import AramWindow


def main():
    global app, interface
    app = QtGui.QApplication(sys.argv)
    app.setStyleSheet("QWidget {background-image: url(../graphics/background.png) }")
    interface = AramWindow()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()