#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# __author__ = 'VR'

from pprint import pprint
from random import choice
from corpus import Corpus
from lsi import Index, LOG_ALL
from tokenizer import TokenizerError
import six
import pickle
from interface_settings import OptionsDialog, Options

"""Slow ops , should be done on another thread ?"""
#optimization to use object saved in file to save time
use_file = True
saved_index = "saved_index.pkl"

max_number_of_results = 200

query = "𐡀𐡁𐡉𐡉 𐡓𐡁𐡀 𐡉𐡀𐡅𐡔 𐡔𐡋𐡀 𐡌𐡃𐡏𐡕"
query = "bonjour"#marche pas
query = "ארמית" #marche pas
query = "דף ו,ב משנה" #marche pas non plus
query = "𐡔𐡁𐡉𐡏𐡉𐡕"


def save_index_to_file():
    pickle.dump(index, open(saved_index, "wb"))


#create index from scratch
def index_new():
    global index
    index = Index(verbose=True)
    corporas = Corpus.get_all_corpus()
    for corpora in corporas:
        for key, src in corpora.sources.items():
            z = zip(src.section_titles, src.sections)
            for sec_title, sec_words in z:
                index.add_document((key, src.title, sec_title), sec_words)
    index.indexing()
    if use_file:
        save_index_to_file()


def load_index_from_file():
    global index
    index = pickle.load(open(saved_index, "rb"))


def create_index():
    if use_file:
        try:
            load_index_from_file()
        except Exception as e:
            print(e)
            index_new()
    else:
        index_new()


def total_number_of_words(documents_values):
    num_words = 0
    for doc in documents_values:
        num_words += len(doc)
    return num_words


def get_doc_key_randomly():
    return choice(list(index.documents.keys()))


def print_index_data():
    # this is actually the number of sections as they are treated each one as separate documents
    number_of_documents = len(index.documents.keys())
    print("Total nb of docs : ", number_of_documents)
    print("total number of words ", total_number_of_words(index.documents.values()))
    random_doc_key = get_doc_key_randomly()
    print("Doc_key chosen at random : ", random_doc_key)
    print(len(index.documents[random_doc_key]), " words in ", random_doc_key)


def count_word_occurences(word, doc_key):
    """how many words in each document"""
    count = 0
    for w in index.documents[doc_key].split():
        if w == word:
            count += 1
    return count


def make_sure_is_list_or_tuple(q):
    """only accept lists or tuples, strings transformed into single item list"""
    if isinstance(q, six.string_types):
        list_query = q.split(" ")
    else:
        assert hasattr(q, "__iter__")
        list_query = q
    return list_query


def join_list(q):
    if isinstance(q, six.string_types):
        return q
    assert hasattr(q, "__iter__")
    return " ".join(q)


def search_index(q):
    try:
        string_search = join_list(q)
        """lsi search method expects a string with words separated by spaces"""
        global max_number_of_results
        max_number_of_results = Options.get_max_word_results_pref()
        documents_found = index.search(string_search, max_number_of_results)
        list_query = make_sure_is_list_or_tuple(q)
        if LOG_ALL:
            print("")
            print("Original query : ", list_query)
            print("")
            print("Results size : ", len(documents_found))

        documents_and_query = dict()
        for d in documents_found:
            documents_and_query[d] = dict()
            for w in list_query:
                documents_and_query[d][w] = count_word_occurences(w, d)
        if LOG_ALL:
            print("*** xml file name, xml title, section title : word : occurence ****************")
            pprint(documents_and_query)
            print("***********")

        return documents_and_query
    except TokenizerError as e:
        print("Tokenizer error : ", e.value)


def get_random_words(max_words_count):
    """chose max_words_count words randomly in the corpus to search them"""
    q = list()
    for i in range(0, max_words_count):
        random_doc_key = get_doc_key_randomly()
        words = index.documents[random_doc_key]
        words_list = words.split()
        random_word = choice(words_list)
        q.append(random_word)
    if LOG_ALL:
        print("random words : ")
        pprint(q)
        print("")

    return q


def search_index_random(max_words_count=Options.get_test_words_number_pref()):
    q = get_random_words(max_words_count)
    return search_index(q)


def init_index():
    create_index()
    print_index_data()


if __name__ == '__main__':
    init_index()
    search_index(dict()) #bogus search
    search_index(query)
    search_index_random()

