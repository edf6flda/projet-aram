#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#__author__ = 'VR'
from PyQt4.QtCore import SIGNAL, SLOT, pyqtSlot
from PyQt4.QtGui import QLineEdit

QString = str


class AramEdit(QLineEdit):

    def __init__(self, parent=None):
        super(QLineEdit, self).__init__(parent)
        self.random_search_text = ""
        self.connect(self, SIGNAL("textChanged(QString)"), self, SLOT("textChanged(QString)"))
        self.connect(self, SIGNAL("textEdited(QString)"), self, SLOT("textEdited(QString)"))
        self.move(30, 50)
        self.resize(300, 40)

    @pyqtSlot(QString)
    def textEdited(self, string):
        print("textEdited : ")
        self.random_search_text = ""
        # print("textEdited : "+string, "text() : "+self.text())

    def get_random_search_text(self):
        return self.random_search_text

    @pyqtSlot(QString)
    def textChanged(self, string):
        print("textChanged : "+str(len(string))+"/"+str(len(self.get_random_search_text())))
        # print("textChanged : "+str(string.encode('utf-32')))
        # print("textChanged : ", str(string), "text() : "+self.text())

    def keyPressEvent(self, QKeyEvent):
        """
        before text changed
        """
        print("QKeyEvent : ", QKeyEvent.text())
        super().keyPressEvent(QKeyEvent)

    def setText(self, p_str):
        self.random_search_text = p_str
        super().setText(p_str)