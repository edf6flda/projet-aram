#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import os
import os.path
from collections import OrderedDict

from source import Source


class Corpus:
    """
    class intended to manage corpora.
    
    """
    def __init__(self, path):
        self.path = path
        self.sources = OrderedDict()
        for nm in os.listdir(path):
            if not nm.endswith('.xml'):
                continue
            self.sources[nm[:-4]] = Source(os.path.join(path, nm))

    def __iter__(self):
        return iter(self.sources.items())

    def __repr__(self):
        return '<Corpus: loc={}, {} sources>'.format(self.path, len(self.sources))

    @staticmethod
    def get_all_corpus(path='../data/'):
        return tuple(Corpus(os.path.join(path, subdir)) for subdir in os.listdir(path))


if __name__ == '__main__':
    corp = Corpus.get_all_corpus()
    print(corp)
