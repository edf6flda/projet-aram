#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#__author__ = 'VR'
import os
from PyQt4.QtCore import QUrl
from PyQt4.QtGui import QListWidget, QDesktopServices, QListWidgetItem, QIcon

FILE_XML = ".xml"

FILE_PATH_PREFIX = "../data/"

LIST_ITEM_ICON = '../graphics/document2.png'


class AramListWidget(QListWidget):

    def __init__(self, parent=None):
        super(AramListWidget, self).__init__(parent)
        self.icon = QIcon(LIST_ITEM_ICON)
        self.itemClicked.connect(self.on_item_clicked)
        self.move(100, 150)
        self.resize(400, 400)

    def mousePressEvent(self, event):
        self._mouse_button = event.button()
        super(AramListWidget, self).mousePressEvent(event)

    def on_item_clicked(self, item):
        print(self._mouse_button)
        if self._mouse_button == 1:
            self.doc_clicked_left()
        elif self._mouse_button == 2:
            self.doc_clicked_right()

    def get_selected_list_item(self):
        return self.selectedItems()[0].text()

    @staticmethod
    def open_url(url):
        q_url = QUrl('file:///' + url)
        print("Will open : "+url)
        QDesktopServices.openUrl(q_url)

    def doc_clicked_left(self):
        """ left click opens xml program with favorite program """
        clicked_list_item_text = self.get_selected_list_item()
        p = clicked_list_item_text[2]
        p2 = clicked_list_item_text[2: 5]
        #we assume file name start at position 2, after the "('"
        #and that filename has 3 letters
        url = os.path.abspath("%s%s/%s%s" % (FILE_PATH_PREFIX, p, p2, FILE_XML))
        self.open_url(url)

    def doc_clicked_right(self):
        """ right click open the folder where the xml file is located """
        clicked_list_item_text = self.get_selected_list_item()
        p = clicked_list_item_text[2]
        url = os.path.abspath("%s%s" % (FILE_PATH_PREFIX, p))
        self.open_url(url)

    def add_item_to_list(self, w_):
        item = QListWidgetItem(self.icon, w_)
        self.addItem(item)