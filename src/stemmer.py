#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from collections import Counter
from bisect import bisect_left
from trie import Trie


def find_ge(a, x):
    'Find leftmost item greater than or equal to x'
    i = bisect_left(a, x)
    if i != len(a):
        return a[i]
    raise ValueError

# intended for debug
def represent(word, sep):
    'represent all "cuts points" of a word'
    l = list(word)
    n = 0
    for i in sep:
        l.insert(i + n, '|')
        n += 1
    return ''.join(l)


class Stemmer:
    def __init__(self):
        self.trained = False

    def train(self, iterable):
        """
        Train the stemmer on word list.
        the training process isn't incremental,
        so stemmer must trained only once
        """
        wordlst = list(iterable)

        if self.trained:
            raise RuntimeError('this stemmer already trained')

        self.counter = Counter(wordlst)

        prefixes_count = Counter(w[:1] for w in wordlst if self.counter[w[1:]] >= 10)
        prefixes_count.update(Counter(w[:2] for w in wordlst if self.counter[w[2:]] >= 10))

        self.commons_prefixes = dict(prefixes_count.most_common(25))

        self.trie = Trie()
        for word in wordlst:
            self.trie.add(word) # unprefix ?

        self.trained = True

    def unprefix(self, word):
        "de-prefixation based on prefixes statistics"
        if not hasattr(self, 'commons_prefixes'):
            raise RuntimeError('that stemmer already trained')

        if word[:2] in self.commons_prefixes \
        and self.counter[word[2:]] >= 3:
            return word[2:]

        if word[:1] in self.commons_prefixes \
        and self.counter[word[1:]] >= 3:
            return word[1:]

        return word

    def unsuffix(self, word):
        "de-suffixation based on suffixes statistics"
        if not self.trained:
            raise RuntimeError("that stemmer hasn't trained")

        v = self.trie.get_hfreqs(word)
        if v is None:
            return word
        
        sep = [i + 1 for i in range(1, len(v) - 1)  if v[i] > 0.65
                                                    and v[i-1] < v[i]
                                                    and v[i+1] < v[i]]

        #print(represent(word, sep), '\n', v); input()

        try:
            i = find_ge(sep, 3)
            return word[:i]
        except ValueError:
            return word

    def stem(self, word):
        nw = self.unsuffix(self.unprefix(word))
        #if len(nw) != len(word):
        #    print(word, nw)
        return nw


if __name__ == '__main__':
    from time import time
    from itertools import chain
    from corpus import Corpus
    # from utils import aramaic2raw_hebrew
    
    corpora = Corpus.get_all_corpus()

    gstemmer = Stemmer()
    gwords = []

    for corpus in corpora:
        #stemmer = Stemmer()
        all_words = []

        #t0 = time()
        #print(' training on', corpus)
        for name, src in corpus.sources.items():
            nm = src.title
            # wlst = [aramaic2raw_hebrew(w) for w in src.words if w.isalpha()]
            wlst = [w for w in src.words if w.isalpha()]
            all_words.extend(wlst)
        #stemmer.train(all_words)
        #print(' {} words added, elapsed {}s'.\
        #      format(len(all_words), time() - t0))

        #S = set(all_words)
        #stemmed = set(stemmer.stem(w) for w in S)
        #print('all words:\t{}'.format(len(S)))
        #print('stemmed words:\t{}'.format(len(stemmed)))

        gwords.append(all_words)

    all_words = list(chain(*gwords))
    gstemmer.train(all_words)

    for lst in gwords:
        S = set(gstemmer.stem(w) for w in set(lst))
        print('total stemmed words:\t{}'.format(len(S)))
    
    gstemmed = set(gstemmer.stem(w) for w in set(all_words))
    print('total stemmed words:\t{}'.format(len(gstemmed)))
    
