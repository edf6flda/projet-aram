#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# __author__ = 'VR'
import PyQt4
from PyQt4.QtGui import QVBoxLayout, QPushButton, QSizePolicy, QLabel, QListWidget, QHBoxLayout, \
    QListWidgetItem
from corpus import Corpus

from trie import Trie


USER_PREF = 'user_pref'

WORDS_NUMBER_PREF = 'max_words_count'
PREF_DEFAULT_RESULTS_COUNT = 20
USE_LSI_PREF = 'use_lsi'
PREF_DEFAULT_USE_LSI = True
MIN_COSINUS_PREF = 'min_cosinus'

trie_initiated = False


def get_default_min_cosinus():
    if PREF_DEFAULT_USE_LSI:
        return 0.3
    else:
        return 0.05


PREF_DEFAULT_MIN_COSINUS = get_default_min_cosinus()

NUMBER_TEST_WORDS = 'number_test_words'
PREF_DEFAULT_NUM_TEST_WORDS = 6

letters_to_search = ""

trie = Trie()


class AramListItem(QListWidgetItem):

    def __init__(self, label, parent=None):
        super(QListWidgetItem, self).__init__(label, parent=parent)
        self.word = label


class AramKeysButton(QPushButton):

    def __init__(self, text='', parent=None, list=None, label=None):
        QPushButton.__init__(self, text, parent)
        self.list = list
        self.label = label
        self.button_letter = text
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.clicked.connect(self.search_letter)

    def search_letter(self):
        global letters_to_search
        letters_to_search += self.button_letter
        print(letters_to_search)
        self.init_or_search(letters_to_search)

    def init_or_search(self, search):
        global trie_initiated
        if not trie_initiated:
            self.list.clear()
            self.list.addItem(AramListItem("Indexing .."))
            corporas = Corpus.get_all_corpus()
            for corpora in corporas:
                for key, src in corpora.sources.items():
                    z = zip(src.section_titles, src.sections)
                    for sec_title, sec_words in z:
                        for w in sec_words.split():
                            if not trie.__contains__(w):
                                trie.add(w)
            trie_initiated = True
        self.search_in_trie(search)

    def search_in_trie(self, search):
        self.list.clear()
        self.list.addItem(AramListItem("Searching .."))
        trie_suffixes = trie.suffixes(search)
        self.list.clear()
        try:
            i = len(trie_suffixes)
            if i <= 0:
                return
            if i <= 10:
                word_list = trie_suffixes
            else:
                word_list = trie_suffixes[:10]
            for w in word_list:
                self.list.addItem(AramListItem(letters_to_search+w))
        except:
            SearchDialog.reset_letters_to_search()
            pass


class SearchDialog(PyQt4.QtGui.QDialog):

    @staticmethod
    def reset_letters_to_search():
        global letters_to_search
        letters_to_search = ""

    def __init__(self, parent):
        super(SearchDialog, self).__init__(parent)
        self.setWindowTitle('Assisted search')
        self.label = QLabel(self)
        self.label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.words_to_search = ""
        self.reset_letters_to_search()

        self.list = QListWidget(self)
        self.list.itemClicked.connect(self.on_item_clicked)
        self.vbox = QVBoxLayout(self)
        self.vbox.addWidget(self.label)
        self.vbox.addWidget(self.list)
        btn = QPushButton('Confirm', self)
        btn.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        btn.clicked.connect(self.btn_clicked)
        self.add_buttons(self.list)
        self.vbox.addWidget(btn)

    def add_item(self, vbox, results_, edit, text):
        label = QLabel(results_, self)
        edit.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        edit.setText(text)
        label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        vbox.addWidget(label)
        vbox.addWidget(edit)

    def add_buttons(self, list):
        charset = [chr(val) for val in range(0x10840, 0x10855 + 1)]

        i = 0
        for c in charset:
            if i % 10 == 0:
                hbox = QHBoxLayout()
                hbox.addStretch(1)
            btn = AramKeysButton(c, self, list)
            hbox.addWidget(btn)
            if i % 10 == 0:
                self.vbox.addStretch(1)
                self.vbox.addLayout(hbox)
            i += 1

    def on_item_clicked(self):
        item_word = self.list.selectedItems()[0].word
        if len(self.words_to_search) == 0:
            self.words_to_search = item_word
        else:
            self.words_to_search = self.words_to_search + " "+item_word
        self.label.setText(self.words_to_search)

    def btn_clicked(self):
        self.hide()

    def get_search_text(self):
        return self.words_to_search


