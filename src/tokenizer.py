#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import re

# adapted from http://docs.python.org/3/library/re.html#writing-a-tokenizer
class Tokenizer:
    def __init__(self, token_spec):
        "create and return tokenizer from rules"
        tok_pattern = '|'.join('(?P<%s>%s)' % pair for pair in token_spec)
        self.rxp = re.compile(tok_pattern, re.DOTALL)
    
    def __call__(self, s):
        pos = 0
        mo = self.rxp.match(s)
        while mo is not None:
            typ = mo.lastgroup
            if typ != 'SKIP':
                val = mo.group(typ)
                yield val
            pos = mo.end()
            mo = self.rxp.match(s, pos)
        if pos != len(s):
            raise TokenizerError('Unexpected character %r' % s[pos])


AramTokenizer = Tokenizer([
    ('NUMBER',  r'\d+'),
    ('TEXT',    r"[\U00010840-\U00010855\"']+"),
    ('PUNCT',   r'[;:.\U00010857]'),
    ('SKIP',    r'.'),     # skip over all unkowns chars
])

class TokenizerError(RuntimeError):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
