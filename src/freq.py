#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from collections import Counter


class Freq:
    """
    Store frequency of some data.
    
    Exemples:
    >>> freq = Freq("azerty")
    >>> freq.update("aqedrf")
    >>> freq['a']
    0.16666666666666666
    >>> freq['x']
    0.0
    >>> del freq['a']
    >>> freq['a']
    0.0
    >>> freq = Freq()
    >>> freq['*']
    0.0
    """
    def __init__(self, iterable=None):
        self.counter = Counter()
        self.n = 0
        if iterable != None:
            self.update(iterable)

    def update(self, iterable):
        self.counter.update(iterable)
        self.n = sum(self.counter.values())

    def __getitem__(self, key):
        "get frequency for a key"
        if self.n == 0:
            return 0.
        return self.counter[key] / self.n

    def __delitem__(self, key):
        "delete a key from the freqency counter"
        del self.counter[key]
        self.n = sum(self.counter.values())


if __name__ == '__main__':
    import doctest
    doctest.testmod()
