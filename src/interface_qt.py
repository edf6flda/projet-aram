#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# __author__ = 'VR'
from pprint import pprint
import sys

from PyQt4.QtCore import SIGNAL, QThread
from PyQt4.QtGui import QPushButton, QMainWindow, QMessageBox, QApplication
from setuptools.compat import unicode

from indexation import init_index, join_list, search_index, get_random_words
from interface_assisted_search import SearchDialog
from interface_widget_list import AramListWidget
from interface_settings import OptionsDialog
from interface_widget_edit import AramEdit


WINDOW_TITLE = "Aram : Aramaic Search Engine"

RUN_SEARCH_STRING = "update(QString)"

index_initiated = False


def init_index_for_session():
    global index_initiated
    if not index_initiated:
        init_index()
        index_initiated = True


class WorkThread(QThread):

    def __init__(self, search_text):
        QThread.__init__(self)
        self.search_text = search_text

    def run(self):
        init_index_for_session()
        self.emit(SIGNAL(RUN_SEARCH_STRING), self.search_text)
        return

    def __del__(self):
        self.wait()


class AramWindow(QMainWindow):
    documents = dict()

    def __init__(self):
        super(AramWindow, self).__init__()
        self.editor = AramEdit(self)
        self.settings_button = QPushButton("Settings", self)
        self.search_button = QPushButton("Search", self)
        self.clear_button = QPushButton("Clear All", self)
        self.assistant_search = QPushButton("Assisted Search", self)
        self.search_button_test = QPushButton("Test Search", self)
        self.list_of_search_result = AramListWidget(self)
        self.init_ui()

    def show_settings_dialog(self):
        dlg = OptionsDialog(self)
        dlg.show()

    def show_assistant_dialog(self):
        dlg = SearchDialog(self)
        dlg.exec_()
        text_to_search = dlg.get_search_text()
        print("text_to_search: ", text_to_search)
        self.editor.setText(text_to_search)


    def init_ui(self):

        self.settings_button.move(30, 10)
        self.settings_button.resize(100, 30)
        self.settings_button.setStyleSheet("color: rgb(50, 50, 250)")

        self.search_button.move(360, 50)
        self.search_button.resize(200, 40)

        self.search_button_test.move(400, 100)
        self.search_button_test.resize(160, 40)
        self.search_button_test.setStyleSheet("color: rgb(0, 200, 170)")

        self.assistant_search.resize(160, 40)
        self.assistant_search.move(215, 100)

        self.clear_button.move(30, 100)
        self.clear_button.resize(160, 40)
        self.clear_button.setStyleSheet("color: rgb(255, 0, 0)")

        self.assistant_search.clicked.connect(self.show_assistant_dialog)
        self.settings_button.clicked.connect(self.show_settings_dialog)
        self.search_button.clicked.connect(self.search_clicked)
        self.clear_button.clicked.connect(self.clear_clicked)
        self.search_button_test.clicked.connect(self.random_search_clicked)

        self.statusBar()
        self.statusBar().showMessage("Type search keywords, separated by space and press 'search'")

        self.show_search_items()
        #x,y,width, height
        self.setGeometry(100, 100, 600, 600)
        self.setWindowTitle(WINDOW_TITLE)
        self.show()

    def show_text_not_found_message(self, editor_text):
        message = "'" + str(editor_text) + "'" + " not found"
        self.statusBar().showMessage(message)
        QMessageBox.information(self, "Search", message)

    def show_error_message(self, string):
        QMessageBox.information(self, "Error", string)

    def show_results(self, editor_text):
        # do search
        self.documents = self.get_documents_searched(join_list(editor_text))
        # remove all previous items
        self.list_of_search_result.clear()
        try:
            assert hasattr(self.documents, "__iter__")
            nb_of_results = len(list(self.documents.keys()))
            if nb_of_results == 0:
                self.show_text_not_found_message(editor_text)
            else:
                if nb_of_results != 1:
                    postfix = " documents"
                else:
                    postfix = " document"
                postfix_found__ = str(nb_of_results) + postfix + " found."
                self.statusBar().showMessage(postfix_found__)
                for doc in self.documents.keys():
                    words = self.documents[doc]
                    w_ = str(doc)+"\n"
                    for w in words:
                        w_ += "               * " + str(w) + " : " + str(words[w])+"\n"
                    self.list_of_search_result.add_item_to_list(w_)
                QMessageBox.information(self, "Search", postfix_found__)
        except AssertionError as e:
            print(e)
            self.show_text_not_found_message(editor_text)
        self.show_search_items()

    def show_searching_message(self):
         self.statusBar().showMessage("Creating index ...")

    def run_thread_before_search(self, editor_text):
        self.show_searching_message()
        search_thread = WorkThread(editor_text)
        self.connect(search_thread, SIGNAL(RUN_SEARCH_STRING), self.show_results)
        search_thread.start()

    def run_thread_before_random_search(self):
        self.show_searching_message()
        search_thread = WorkThread(None)
        self.connect(search_thread, SIGNAL(RUN_SEARCH_STRING), self.random_search)
        search_thread.start()

    def run_thread_or_search(self, editor_text):
        if not index_initiated:
            self.run_thread_before_search(editor_text)
        else:
            self.show_results(editor_text)

    def editor_search(self, editor_text):
        self.statusBar().showMessage('Searching for : ' + join_list(editor_text)+" ... ")
        self.run_thread_or_search(editor_text)

    def search_clicked(self):
        editor_text = self.editor.get_random_search_text()
        if len(editor_text) < 1:
            editor_text = self.editor.text()
        pprint(editor_text)
        print("search_clicked type(unicode", type(unicode(self.editor.text())))
        if len(editor_text) > 0:
            self.hide_search_items()
            #error with this string UnicodeEncodeError: 'utf-8' codec can't encode character '\ud802' in position 0: surrogates not allowed
            self.editor_search(editor_text)
        else:
            self.statusBar().showMessage("Type at least one keyword and press 'search'")
            # self.show_error_dialog("Type at least one keyword and press 'search'")


    def random_search(self):
        """chose 5 random words to test search engine"""
        q = get_random_words(5)
        s = join_list(q)
        # self.editor.random_search_text = s
        self.editor.setText(s)
        self.statusBar().showMessage('Searching for : ' + s + " ... ")
        self.show_results(q)

    def random_search_clicked(self):
        self.hide_search_items()
        self.statusBar().showMessage("Random search ...")
        if index_initiated:
            self.random_search()
        else:
            self.run_thread_before_random_search()

    def clear_clicked(self):
        self.statusBar().showMessage("Type search keywords, separated by space and press 'search'")
        self.show_search_items()
        self.list_of_search_result.clear()
        self.editor.clear()
        self.editor.random_search_text = ""

    @staticmethod
    def get_documents_searched(search_text_get):
        return search_index(search_text_get)

    def hide_search_items(self):
        self.list_of_search_result.setDisabled(False)
        self.search_button.setDisabled(True)
        self.search_button_test.setDisabled(True)
        self.editor.setDisabled(True)

    def show_search_items(self):
        #self.list_of_search_result.setDisabled(True)
        self.search_button.setDisabled(False)
        self.search_button_test.setDisabled(False)
        self.editor.setDisabled(False)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyleSheet("QWidget {background-image: url(../graphics/background.png) }")
    interface = AramWindow()
    sys.exit(app.exec_())






