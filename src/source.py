#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from itertools import chain
from lxml.etree import parse as parse_xml

from tokenizer import AramTokenizer

class Source:
    """
    class intended to manage aramaic xml sources.
    all properties are lazy, ie. they load data
    only when it's required
    """
    def __init__(self, path):
        self.path = path
    
    def free_memory(self):
        "free all allocated memory for data (depend on GC)"
        self.__dict__ = {'path': self.path}

    def __repr__(self):
        return '<Source: loc={}>'.format(self.path)
        
    def _xml(self):
        "parse and store xml tree from source"
        if not hasattr(self, '_xmlobj'):
            self._xmlobj = parse_xml(self.path)
        return self._xmlobj
    
    def _title(self):
        "get source title"
        if not hasattr(self, '_xmltitle'):
            self._xmltitle = self.xml.getroot().find('title').text
        return self._xmltitle

    def _data(self):
        "get raw text"
        if not hasattr(self, '_rawdata'):
            self._rawdata = '\n'.join(self.sections)
        return self._rawdata

    def _sections(self):
        "get raw text sections"
        if not hasattr(self, '_xmlsections'):
            # remark: invalid for q
            self._xmlsections = self.xml.xpath('//section[@page]/text()')
        return self._xmlsections

    def _section_titles(self):
        if not hasattr(self, '_xmlsectiontitles'):
            self._xmlsectiontitles = [e.get('page')
                                          for e in self.xml.xpath('//section[@page]')]
        return self._xmlsectiontitles

    def _segments(self):
        "get sections segmented by words"
        if not hasattr(self, '_toksegments'):
            self._toksegments = [list(AramTokenizer(segment))
                                     for segment in self.sections]
        return self._toksegments

    def _words(self):
        "get all segmented words"
        if not hasattr(self, '_tokwords'):
            self._tokwords = list(chain(*self.segments))
        return self._tokwords
    
    xml = property(fget=_xml)
    title = property(fget=_title)
    data = property(fget=_data)
    sections = property(fget=_sections)
    section_titles = property(fget=_section_titles)
    segments = property(fget=_segments)
    words = property(fget=_words)
