#!/bin/bash
#tested on ubuntu 14.04
#This script installs and launches aram

sudo apt-get install python3 python3-lxml
sudo apt-get install build-essential python3-dev python3-numpy python3-setuptools python3-scipy libatlas-dev libatlas3-base
sudo apt-get install python3-matplotlib
sudo apt-get install python3-pip
sudo pip3 install git+https://github.com/scikit-learn/scikit-learn.git
sudo apt-get install python-qt4 pyqt4-dev-tools
git clone git@gitorious.org:edf6flda/projet-aram.git
cd projet-aram/src
python3 start.py
